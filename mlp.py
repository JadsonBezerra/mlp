import random
import numpy as np
import math
class neuronio:
    def __init__(self,pesos):
        self.pesos=pesos
        self.erro = 0
    def passar(self,entradas):
        saida = 0
        #print('entradas',entradas)
        for i in range(len(entradas)):
            saida += entradas[i] * self.pesos[i]
        #print('pesos',self.pesos)
        return saida
    def atualizarPesos(self,taxaAprendizado,entradas):
        for i in range(len(entradas)):
            self.pesos[i]+= self.erro*entradas[i]*taxaAprendizado


class camada:
    def __init__(self,tamanho,numeroEntradas):
        self.neuronios = [neuronio(np.random.uniform(low = 0,high = 1, size = numeroEntradas)) for i in range(tamanho)]
        self.tamanho = tamanho
    def passar(self,entradas):
        saidas = []
        for i in range(self.tamanho):
            saidas.append(self.neuronios[i].passar(entradas))
        return saidas


class MLP:
    def __init__(self, tamanhoCamadas, taxaAprendizado = 0.25):
        self.camadas = [camada(tamanhoCamadas[i],tamanhoCamadas[i-1]+1) for i in range(1,len(tamanhoCamadas))]
        self.taxaAprendizado = taxaAprendizado
    

    def feedFoward(self,xi):
        listaEntradas = []
        listaSaidas = []
        
        entradasAtuais = xi.copy()
        for camada in self.camadas:
            listaEntradas.append(entradasAtuais.copy())
            saidasAtuais = camada.passar(entradasAtuais)
            listaSaidas.append(saidasAtuais.copy())
            entradasAtuais =[self.ativacao(saida) for saida in saidasAtuais]
        
        listaEntradas.append(entradasAtuais)

        return listaEntradas,listaSaidas
    

    def backPropagation(self,xi,yi):
        listaEntradas,listaSaidas = self.feedFoward(xi)
        for i in range(len([listaEntradas[-1]])):
            self.camadas[-1].neuronios[i].erro = yi[i] - listaEntradas[-1][i]
            # self.camadas[-1].neuronios[i].erro *= listaSaidas[-1][i]*(1-listaSaidas[-1][i])
        for i in range(len(self.camadas) - 2 , -1,-1):
            for j in range(len(self.camadas[i].neuronios)):
                erroNeuronio = 0
                for k in range(len(self.camadas[i + 1].neuronios)):
                    # print('peso',self.camadas[i+1].neuronios[k].pesos[j])
                    # print('erro',self.camadas[i+1].neuronios[k].erro)
                    erroNeuronio += (self.camadas[i+1].neuronios[k].pesos[j] * self.camadas[i+1].neuronios[k].erro)
                self.camadas[i].neuronios[j].erro = erroNeuronio * self.derivadaAtivacao(listaSaidas[i][j])

        for i in range(len(self.camadas)):
            for j in range(len(self.camadas[i].neuronios)):
                self.camadas[i].neuronios[j].atualizarPesos(self.taxaAprendizado,listaEntradas[i])
        

    def train(self,X,Y,iteracoes = 5000):
        for i in range(iteracoes):
            print(100.0*i/iteracoes,'%')
            X,Y = self.embaralhar(X,Y)
            for xi,yi in zip(X,Y):
                self.backPropagation(xi,[yi] if type(yi)!=list else yi)

    def embaralhar(self,X,Y):
        Xnovo = X.copy()
        Ynovo = Y.copy()
        for i in range(random.randint(len(Y)/2,len(Y))):
            linha1 = random.randint(0,len(Y)-1)
            linha2 = random.randint(0,len(Y)-1)

            Ynovo[linha1] = Y[linha2]
            Ynovo[linha2] = Y[linha1]

            Xnovo[linha1] = X[linha2]
            Xnovo[linha2] = X[linha1]

        return Xnovo,Ynovo


    def predict(self,xi):
        listaEntradas, listaSaidas = self.feedFoward(xi)
        return listaEntradas[-1]

    def ativacao(self, z):
        # return 1.0 / (1.0 + np.exp(-z))
        return max(0,z)
    def derivadaAtivacao(self, z):
        # return self.ativacao(z) * (1 - self.ativacao(z))
        return (z>0)*1

    def salvarPesos(self):
        arquivo = open('mlp.weights','w')
        for camada in self.camadas:
            for neuronio in camada.neuronios:
                for i in range(len(neuronio.pesos)):
                    arquivo.write(str(neuronio.pesos[i]))
                    if i != len(neuronio.pesos)-1:
                        arquivo.write(' ')
                arquivo.write('\n')
    
    def carregarPesos(self,nomeArquivo):
        # r = np.loadtxt(open(nomeArquivo),delimiter=' ')
        arquivo = open(nomeArquivo,'r')
        i=0
        numeros = []
        for linha in arquivo:
            for numero in linha.split(' '):
                numeros.append((float(numero)))
        for camada in self.camadas:
            for neuronio in camada.neuronios:
                for peso in neuronio.pesos:
                    peso = numeros[i]
        

def binario(numero):
    binario = bin(int(numero))
    tamanho = len(binario)
    digito1 = binario[-1]
    digito2 = binario[-2] if tamanho>3 else 0 
    digito3 = binario[-3] if tamanho>4 else 0 
    return [ int(digito3), int(digito2), int(digito1)]

# r = np.loadtxt(open('data_banknote_authentication.txt'),delimiter=',')
r = np.loadtxt(open('EEG_EYE_STATE.arff'),  delimiter=',')

# mlp = MLP([4,3,1],0.1)

# X = r[200:-200,0:4].tolist()

# Y = r[200:-200,4].tolist()
#mlp = MLP([2,1,1])]

mlp = MLP([14,12,1],0.1)


X = r[1000:-1000,0:14].tolist()

Y = r[1000:-1000,14].tolist()

mlp.train(X,Y,100)
# mlp.salvarPesos()

# mlp.carregarPesos('mlp-eye-1.weights')

# for camada in mlp.camadas:
    # for neuronio in camada.neuronios:
        # print(neuronio.pesos)

def arredondar(x):
    return 1 if x>0.5 else 0

erro =0
for x,y in zip(X[-1000:-1],Y[-1000:-1]):
    print('entrada: ',x)
    print('saida da rede: ',mlp.predict(x))
    print('saida esperada: ',y)
    for saida in mlp.predict(x):
        erro += (y - arredondar(saida))**2 


for x,y in zip(X[0:1000],Y[0:1000]):
    print('entrada: ',x)
    print('saida da rede: ',mlp.predict(x))
    print('saida esperada: ',y)
    for saida in mlp.predict(x):
        erro += (y - arredondar(saida))**2 


print ('erro:',erro*100.0/2000,'%')



# print(mlp.predict([67.0,1.0,4.0,160.0,286.0,0.0,2.0,108.0,1.0,1.5,2.0,3.0,3.0])) #2
# print(mlp.predict([63.0,1.0,1.0,145.0,233.0,1.0,2.0,150.0,0.0,2.3,3.0,0.0,6.0])) #0
# print(mlp.predict([67.0,1.0,4.0,120.0,229.0,0.0,2.0,129.0,1.0,2.6,2.0,2.0,7.0])) #1

#mlp.train([[0,0],[1,0],[0,1],[1,1]],[0,1,1,1],2000)

#print(mlp.predict([0,0]))
#print(mlp.predict([0,1]))
#print(mlp.predict([1,0]))
#print(mlp.predict([1,1]))